package kehdo.mmfd.com.kehdo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by gulafshan on 04/03/2017.
 */
public class Parse {
    private static Parse ourInstance = new Parse();

    public static Parse getInstance() {
        return ourInstance;
    }

    private Parse() {
    }


    public JSONArray parseMultipleQueries(JSONObject jsonObject) {
        if (jsonObject == null)
            return null;

        List<Legislation> Legislations = new ArrayList<Legislation>();
        JSONArray results = jsonObject.optJSONArray("results");
        if (results == null)
            return null;
        else
            return results;
    }

    public List<Legislation> parseLegislations(JSONObject jsonObject) {
        if (jsonObject == null)
            return null;

        List<Legislation> results = new ArrayList<Legislation>();
        JSONArray hits = jsonObject.optJSONArray("hits");
        if (hits == null)
            return null;

        for (int i = 0; i < hits.length(); ++i) {
            JSONObject hit = hits.optJSONObject(i);
            if (hit == null)
                continue;

            // Legislation movie = parseLegislationObj(hit);
            //results.add(movie);

        }
        return results;
    }

    public List<Legislation> parseLegislationObj(JSONObject jsonObject) {
        if (jsonObject == null)
            return null;

        List<Legislation> legislationList = new ArrayList<Legislation>();
        Iterator<String> iterator = jsonObject.keys();

        while (iterator.hasNext()) {

            String key = iterator.next();

            try {
                if (jsonObject.get(key) instanceof JSONObject) {

                    JSONObject obj1 = (JSONObject) jsonObject.get(key);

                    Legislation legislation = new Legislation();

                    legislation.setId(obj1.optString("ID").toString());
                    legislation.setTitle(obj1.optString("Title").toString());
                    legislation.setInfo(obj1.optString("Info").toString());
                    legislation.setName(obj1.optString("Name").toString());

                    System.out.println("LEGISLATION : " + legislation.getTitle());

                    JSONObject answers = obj1.optJSONObject("Answers");
                    JSONObject agreeAnswers = answers.optJSONObject("Agree");
                    JSONObject disagreeAnswers = answers.optJSONObject("Disagree");

                    Iterator<String> it = agreeAnswers.keys();
                    while (it.hasNext()) {
                        String key1 = it.next();
                        legislation.agree.add(agreeAnswers.optString(key1));
                    }

                    Iterator<String> itd = agreeAnswers.keys();
                    while (itd.hasNext()) {
                        String key1 = itd.next();
                        legislation.disagree.add(disagreeAnswers.optString(key1));
                    }

                    legislationList.add(legislation);

//                    "Answers":{
//                        "Agree":{
//                            "Reason1":"Just loved the idea should be implemented as quickle as possible",
//                                    "Reason2":"I think its fine"
//                        },
//                        "Disagree":{
//                            "Reason1":"Still need some convincing",
//                                    "Reason2":"I just dont like the idea"
//                        }
//                    },


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return legislationList;
    }


}
