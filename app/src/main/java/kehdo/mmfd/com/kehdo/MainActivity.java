package kehdo.mmfd.com.kehdo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ProgressDialog progressDialog = null;
    private Context context;
    private SessionManager mSessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        mSessionManager = new SessionManager(this);

//        GetRepInfoTask task = new GetRepInfoTask(context, "", "");
//        task.execute();

        GetLegislationDataTask task1 = new GetLegislationDataTask(context, "", "");
        task1.execute();

    }


}
